
plugins {
    application
    kotlin("jvm") version "1.2.60"
}

application {
    mainClassName = "com.wefine.game.MainKt"
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

group = "com.wefine.game"
version = "1.0"

repositories {
    mavenLocal()
    maven {
        url =  uri("http://maven.aliyun.com/nexus/content/groups/public/")
    }
    jcenter()
}

dependencies {
    compile(kotlin("stdlib-jdk8"))
    testCompile("junit", "junit", "4.12")
}
