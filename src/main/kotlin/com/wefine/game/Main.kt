package com.wefine.game

import javafx.application.Application
import javafx.scene.input.KeyEvent
import org.itheima.kotlin.game.core.Window

class Main : Window() {
    override fun onCreate() {
        //窗体创建回调
        println("onCreate")
    }

    override fun onDisplay() {
        //显示刷新回调
        println("onDisplay")
    }

    override fun onKeyPressed(event: KeyEvent) {
        // 按键响应回调
        println("onKeyPressed")
    }

    override fun onRefresh() {
        // 耗时操作回调
        println("onRefresh")
    }
}

fun main(args: Array<String>) {
    Application.launch(Main::class.java)
}